#### 作業目的
御殿山ICXとDSSWとの接続の帯域を2Gから5Gに増やすため、リンクアグリゲーションのメンバーにポートを追加する。

---

#### 作業日時
2017/08/03(木)
作業開始時間未定

---

#### 作業概要
- 1-1. VLANの設定
- 1-2. VLANの設定確認
- 2-1. LAGの設定
- 2-2. LAGの設定確認
- 3-1. 2/1/3 インターフェースの有効化
- 3-2. インターフェースの確認
- 3-3. LAGの状態確認
- 4-1. 2/1/4 インターフェースの有効化
- 4-2. インターフェースの確認
- 4-3. LAGの状態確認
- 5-1. 2/1/5 インターフェースの有効化
- 5-2. インターフェースの確認
- 5-3. LAGの状態確認

---

#### 対象機器
- 対象機器：ICX6610-GDC(202.231.201.215/32)
- 使用するポート：2/1/3, 2/1/4, 2/1/5
- 所属予定のVLAN：181, 182, 186, 198, 207, 220, 225, 227, 2038, 2039, 2104, 2545, ,2743, 2782
- LACP モード：active
- LACP 送信間隔：Long
- Key ID：21100

---

#### 前提条件
- ICX6610 Version:7系(8系は設定が異なる)
- 事前に対象機器の追加予定のインターフェースがシャットダウンされている。
- 事前に対向の機器でLAGの設定が投入されている。
- 事前に対向の機器と結線されている。

---
#### 0-1. インターフェースのステータス確認(事前作業)
```
telnet@ICX6610-GDC#show interfaces brief

Port    Link    State   Dupl Speed Trunk Tag Pvid Pri MAC            Name
2/1/1   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/2   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/3   Disable None    None None  None  No  1    0   748e.f8e8.8a92
2/1/4   Disable None    None None  None  No  1    0   748e.f8e8.8a93
2/1/5   Disable None    None None  None  No  1    0   748e.f8e8.8a94
2/1/6   Disable None    None None  None  No  1    0   748e.f8e8.8a95
2/1/7   Disable None    None None  None  No  1    0   748e.f8e8.8a96
2/1/8   Disable None    None None  None  No  1    0   748e.f8e8.8a97
2/1/9   Disable None    None None  None  No  1    0   748e.f8e8.8a98
2/1/10  Disable None    None None  None  No  1    0   748e.f8e8.8a99
2/1/11  Disable None    None None  None  No  1    0   748e.f8e8.8a9a
2/1/12  Disable None    None None  None  No  1    0   748e.f8e8.8a9b
2/1/13  Disable None    None None  None  No  1    0   748e.f8e8.8a9c
2/1/14  Disable None    None None  None  No  1    0   748e.f8e8.8a9d
2/1/15  Disable None    None None  None  No  1    0   748e.f8e8.8a9e
2/1/16  Disable None    None None  None  No  1    0   748e.f8e8.8a9f
2/1/17  Disable None    None None  None  No  1    0   748e.f8e8.8aa0
2/1/18  Disable None    None None  None  No  1    0   748e.f8e8.8aa1
2/1/19  Disable None    None None  None  No  1    0   748e.f8e8.8aa2
2/1/20  Disable None    None None  None  No  1    0   748e.f8e8.8aa3
2/1/21  Disable None    None None  None  No  1    0   748e.f8e8.8aa4
2/1/22  Disable None    None None  None  No  1    0   748e.f8e8.8aa5
2/1/23  Disable None    None None  None  No  1    0   748e.f8e8.8aa6
2/1/24  Disable None    None None  None  No  1    0   748e.f8e8.8aa7
2/2/1   Down    None    None None  None  No  N/A  0   748e.f8e8.8aa9
2/2/2   Down    None    None None  None  No  N/A  0   748e.f8e8.8aaa
2/2/6   Down    None    None None  None  No  N/A  0   748e.f8e8.8aab
2/2/7   Down    None    None None  None  No  N/A  0   748e.f8e8.8aac
2/3/1   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8aad  to-ICX66
2/3/2   Disable None    None None  None  No  1    0   748e.f8e8.8aae
2/3/3   Up      Forward Full 10G   None  No  184  0   748e.f8e8.8aaf  VDC-vMot
2/3/4   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8ab0  EX4200-G
2/3/5   Disable None    None None  None  No  1    0   748e.f8e8.8ab1
2/3/6   Disable None    None None  None  No  1    0   748e.f8e8.8ab2
2/3/7   Disable None    None None  None  No  1    0   748e.f8e8.8ab3
2/3/8   Disable None    None None  None  No  1    0   748e.f8e8.8ab4
mgmt1   Down    None    None None  None  No  None 0   748e.f8e8.8a90
```
@[3-8](コメントは[]の右に記述します。)
---
#### 0-2. VLANの設定確認(事前作業)
```
telnet@ICX6610-GDC#show vlan
Total PORT-VLAN entries: 17
Maximum PORT-VLAN entries: 64

Legend: [Stk=Stack-Id, S=Slot]

PORT-VLAN 1, Name DEFAULT-VLAN, Priority level0, Spanning tree Off
 Untagged Ports: (U2/M1)   3   4   5   6   7   8   9  10  11  12  13  14
 Untagged Ports: (U2/M1)  15  16  17  18  19  20  21  22  23  24
 Untagged Ports: (U2/M3)   2   5   6   7   8
   Tagged Ports: None
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 181, Name FB-DDC-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 182, Name FB-NW-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 184, Name FB-vMotionSW, Priority level0, Spanning tree Off
 Untagged Ports: (U2/M3)   3
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 186, Name FB-B3, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 198, Name FB-STORAGE-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 207, Name FB-ME19, Priority level0, Spanning tree On
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 220, Name FB-B3-UPSTREAM, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 223, Name BEK-ASA-GOTE, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M3)   1   4
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 225, Name FB-VDC, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 227, Name FB-ME22, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2038, Name FB-TEST-VDOM-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2039, Name FB-TEST-VDOM-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2104, Name FB-GTYT, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2545, Name FB-ME19, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2743, Name FB-ME16-PRIMARY-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2782, Name FB-ME22, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
```
---

#### 0-3. LAGのステータス確認(事前作業)
```
telnet@ICX6610-GDC#show link-aggregate
System ID: 748e.f8e8.8a90
Long  timeout: 90, default: 90
Short timeout: 3, default: 3
Port  [Sys P] [Port P] [  Key ] [Act][Tio][Agg][Syn][Col][Dis][Def][Exp][Ope]
2/1/1       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/2       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
```
---
##### 0-3. LAGのステータス確認(事前作業)
```
telnet@ICX6610-GDC# show trunk

Configured trunks:

Trunk ID: 257
Hw Trunk ID: 1
Ports_Configured: 2
Primary Port Monitored: Jointly

Ports   PortName Port_Status Monitor Rx_Mirr Tx_Mirr Monitor_Dir
2/1/1   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/2   FB-VDC*  enable      off     N/A     N/A     N/A

Operational trunks:

Trunk ID: 257
Hw Trunk ID: 1
Duplex: Full
Speed: 1G
Tag: Yes
Priority: level0
Active Ports: 2

Ports   Link_Status port_state LACP_Status
2/1/1   active      Forward    ready
2/1/2   active      Forward    ready
```

---

#### 1-1. VLANの設定
```
conf t
vlan 181 name FB-DDC-MNG by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 182 name FB-NW-MNG by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 186 name FB-B3 by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 198 name FB-STORAGE-MNG by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 207 name FB-ME19 by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 220 name FB-B3-UPSTREAM by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 225 name FB-VDC by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 227 name FB-ME22 by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2038 name FB-TEST-VDOM-LOCAL by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2039 name FB-TEST-VDOM-LOCAL by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2104 name FB-GTYT by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2545 name FB-ME19 by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2743 name FB-ME16-PRIMARY-LOCAL by port
 tagged ethe 2/1/3 to 2/1/5
!
vlan 2782 name FB-ME22 by port
 tagged ethe 2/1/3 to 2/1/5
```
---
#### 1-2. VLANの設定後の期待値
以下のVLANに2/1/1 から 2/1/5までのポートが所属していればOK
```
VLAN 181
VLAN 182
VLAN 186
VLAN 198
VLAN 207
VLAN 220
VLAN 225
VLAN 227
VLAN 2038
VLAN 2039
VLAN 2104
VLAN 2545
VLAN 2743
VLAN 2782
```
+++
```
telnet@ICX6610-GDC#show vlan
Total PORT-VLAN entries: 17
Maximum PORT-VLAN entries: 64

Legend: [Stk=Stack-Id, S=Slot]

PORT-VLAN 1, Name DEFAULT-VLAN, Priority level0, Spanning tree Off
 Untagged Ports: (U2/M1)   6   7   8   9  10  11  12  13  14
 Untagged Ports: (U2/M1)  15  16  17  18  19  20  21  22  23  24
 Untagged Ports: (U2/M3)   2   5   6   7   8
   Tagged Ports: None
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 181, Name FB-DDC-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 182, Name FB-NW-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 184, Name FB-vMotionSW, Priority level0, Spanning tree Off
 Untagged Ports: (U2/M3)   3
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 186, Name FB-B3, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 198, Name FB-STORAGE-MNG, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 207, Name FB-ME19, Priority level0, Spanning tree On
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 220, Name FB-B3-UPSTREAM, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 223, Name BEK-ASA-GOTE, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M3)   1   4
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 225, Name FB-VDC, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 227, Name FB-ME22, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2038, Name FB-TEST-VDOM-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2039, Name FB-TEST-VDOM-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2104, Name FB-GTYT, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2545, Name FB-ME19, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2743, Name FB-ME16-PRIMARY-LOCAL, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
PORT-VLAN 2782, Name FB-ME22, Priority level0, Spanning tree Off
 Untagged Ports: None
   Tagged Ports: (U2/M1)   1   2   3   4   5
   Tagged Ports: (U2/M3)   1
   Uplink Ports: None
 DualMode Ports: None
 Mac-Vlan Ports: None
     Monitoring: Disabled
```
---
#### 2-1. LAGの設定

```
conf t
interface ethernet 2/1/3 to 2/1/5
 port-name FB-VDC-B3
 link-aggregate configure key 21100
 link-aggregate active
```
---

#### 3-1. インターフェースの有効化(LAGの設定ミスを考慮し、1ポートずつenableにする)

```
conf t
interface ethernet 2/1/3
 enable
```

---

#### 3-2. インターフェースの期待値
enableにしたポートが以下の状態であればOK
```
Link = Up,
State = Forward,
Dupl = Full,
Speed = 1G,
Trunk = 257,
Tag = Yes,
MAC = 748e.f8e8.8a90
```
+++
```
telnet@ICX6610-GDC#show interfaces brief

Port    Link    State   Dupl Speed Trunk Tag Pvid Pri MAC            Name
2/1/1   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/2   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/3   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/4   Disable None    None None  None  No  1    0   748e.f8e8.8a93
2/1/5   Disable None    None None  None  No  1    0   748e.f8e8.8a94
2/1/6   Disable None    None None  None  No  1    0   748e.f8e8.8a95
2/1/7   Disable None    None None  None  No  1    0   748e.f8e8.8a96
2/1/8   Disable None    None None  None  No  1    0   748e.f8e8.8a97
2/1/9   Disable None    None None  None  No  1    0   748e.f8e8.8a98
2/1/10  Disable None    None None  None  No  1    0   748e.f8e8.8a99
2/1/11  Disable None    None None  None  No  1    0   748e.f8e8.8a9a
2/1/12  Disable None    None None  None  No  1    0   748e.f8e8.8a9b
2/1/13  Disable None    None None  None  No  1    0   748e.f8e8.8a9c
2/1/14  Disable None    None None  None  No  1    0   748e.f8e8.8a9d
2/1/15  Disable None    None None  None  No  1    0   748e.f8e8.8a9e
2/1/16  Disable None    None None  None  No  1    0   748e.f8e8.8a9f
2/1/17  Disable None    None None  None  No  1    0   748e.f8e8.8aa0
2/1/18  Disable None    None None  None  No  1    0   748e.f8e8.8aa1
2/1/19  Disable None    None None  None  No  1    0   748e.f8e8.8aa2
2/1/20  Disable None    None None  None  No  1    0   748e.f8e8.8aa3
2/1/21  Disable None    None None  None  No  1    0   748e.f8e8.8aa4
2/1/22  Disable None    None None  None  No  1    0   748e.f8e8.8aa5
2/1/23  Disable None    None None  None  No  1    0   748e.f8e8.8aa6
2/1/24  Disable None    None None  None  No  1    0   748e.f8e8.8aa7
2/2/1   Down    None    None None  None  No  N/A  0   748e.f8e8.8aa9
2/2/2   Down    None    None None  None  No  N/A  0   748e.f8e8.8aaa
2/2/6   Down    None    None None  None  No  N/A  0   748e.f8e8.8aab
2/2/7   Down    None    None None  None  No  N/A  0   748e.f8e8.8aac
2/3/1   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8aad  to-ICX66
2/3/2   Disable None    None None  None  No  1    0   748e.f8e8.8aae
2/3/3   Up      Forward Full 10G   None  No  184  0   748e.f8e8.8aaf  VDC-vMot
2/3/4   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8ab0  EX4200-G
2/3/5   Disable None    None None  None  No  1    0   748e.f8e8.8ab1
2/3/6   Disable None    None None  None  No  1    0   748e.f8e8.8ab2
2/3/7   Disable None    None None  None  No  1    0   748e.f8e8.8ab3
2/3/8   Disable None    None None  None  No  1    0   748e.f8e8.8ab4
mgmt1   Down    None    None None  None  No  None 0   748e.f8e8.8a90
```

+++
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK
```
[Sys P] = 1,
[Port P] = 1,
[Key] = 21100,
[Act] = Yes,
[Tio] = L,
[Agg] = Agg,
[Syn] = Syn,
[Col] = Col,
[Dis] = Dis,
[Def] = Def,
[Exp] = Exp,
[Ope] = Ope
```
+++
```
telnet@ICX6610-GDC#show link-aggregate
System ID: 748e.f8e8.8a90
Long  timeout: 90, default: 90
Short timeout: 3, default: 3
Port  [Sys P] [Port P] [  Key ] [Act][Tio][Agg][Syn][Col][Dis][Def][Exp][Ope]
2/1/1       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/2       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/3       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
```
+++
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK
```
Link_Status = active,
port_state = Forward,
LACP_Status = ready
```
+++
```
telnet@ICX6610-GDC# show trunk

Configured trunks:

Trunk ID: 257
Hw Trunk ID: 1
Ports_Configured: 5
Primary Port Monitored: Jointly

Ports   PortName Port_Status Monitor Rx_Mirr Tx_Mirr Monitor_Dir
2/1/1   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/2   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/3   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/4   FB-VDC*  disable     off     N/A     N/A     N/A
2/1/5   FB-VDC*  disable     off     N/A     N/A     N/A


Operational trunks:

Trunk ID: 257
Hw Trunk ID: 1
Duplex: Full
Speed: 1G
Tag: Yes
Priority: level0
Active Ports: 3

Ports   Link_Status port_state LACP_Status
2/1/1   active      Forward    ready
2/1/2   active      Forward    ready
2/1/3   active      Forward    ready
```

---

#### 4-1. インターフェースの有効化(LAGの設定ミスを考慮し、1ポートずつenableにする)

```
conf t
interface ethernet 2/1/4
 enable
```

+++

#### 4-2. インターフェースの期待値
enableにしたポートが以下の状態であればOK
```
Link = Up,
State = Forward,
Dupl = Full,
Speed = 1G,
Trunk = 257,
Tag = Yes,
MAC = 748e.f8e8.8a90
```
+++

```
telnet@ICX6610-GDC#show interfaces brief

Port    Link    State   Dupl Speed Trunk Tag Pvid Pri MAC            Name
2/1/1   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/2   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/3   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/4   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/5   Disable None    None None  None  No  1    0   748e.f8e8.8a94
2/1/6   Disable None    None None  None  No  1    0   748e.f8e8.8a95
2/1/7   Disable None    None None  None  No  1    0   748e.f8e8.8a96
2/1/8   Disable None    None None  None  No  1    0   748e.f8e8.8a97
2/1/9   Disable None    None None  None  No  1    0   748e.f8e8.8a98
2/1/10  Disable None    None None  None  No  1    0   748e.f8e8.8a99
2/1/11  Disable None    None None  None  No  1    0   748e.f8e8.8a9a
2/1/12  Disable None    None None  None  No  1    0   748e.f8e8.8a9b
2/1/13  Disable None    None None  None  No  1    0   748e.f8e8.8a9c
2/1/14  Disable None    None None  None  No  1    0   748e.f8e8.8a9d
2/1/15  Disable None    None None  None  No  1    0   748e.f8e8.8a9e
2/1/16  Disable None    None None  None  No  1    0   748e.f8e8.8a9f
2/1/17  Disable None    None None  None  No  1    0   748e.f8e8.8aa0
2/1/18  Disable None    None None  None  No  1    0   748e.f8e8.8aa1
2/1/19  Disable None    None None  None  No  1    0   748e.f8e8.8aa2
2/1/20  Disable None    None None  None  No  1    0   748e.f8e8.8aa3
2/1/21  Disable None    None None  None  No  1    0   748e.f8e8.8aa4
2/1/22  Disable None    None None  None  No  1    0   748e.f8e8.8aa5
2/1/23  Disable None    None None  None  No  1    0   748e.f8e8.8aa6
2/1/24  Disable None    None None  None  No  1    0   748e.f8e8.8aa7
2/2/1   Down    None    None None  None  No  N/A  0   748e.f8e8.8aa9
2/2/2   Down    None    None None  None  No  N/A  0   748e.f8e8.8aaa
2/2/6   Down    None    None None  None  No  N/A  0   748e.f8e8.8aab
2/2/7   Down    None    None None  None  No  N/A  0   748e.f8e8.8aac
2/3/1   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8aad  to-ICX66
2/3/2   Disable None    None None  None  No  1    0   748e.f8e8.8aae
2/3/3   Up      Forward Full 10G   None  No  184  0   748e.f8e8.8aaf  VDC-vMot
2/3/4   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8ab0  EX4200-G
2/3/5   Disable None    None None  None  No  1    0   748e.f8e8.8ab1
2/3/6   Disable None    None None  None  No  1    0   748e.f8e8.8ab2
2/3/7   Disable None    None None  None  No  1    0   748e.f8e8.8ab3
2/3/8   Disable None    None None  None  No  1    0   748e.f8e8.8ab4
mgmt1   Down    None    None None  None  No  None 0   748e.f8e8.8a90
```

---
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK
```
[Sys P] = 1,
[Port P] = 1,
[Key] = 21100,
[Act] = Yes,
[Tio] = L,
[Agg] = Agg,
[Syn] = Syn,
[Col] = Col,
[Dis] = Dis,
[Def] = Def,
[Exp] = Exp,
[Ope] = Ope
```
+++
```
telnet@ICX6610-GDC#show link-aggregate
System ID: 748e.f8e8.8a90
Long  timeout: 90, default: 90
Short timeout: 3, default: 3
Port  [Sys P] [Port P] [  Key ] [Act][Tio][Agg][Syn][Col][Dis][Def][Exp][Ope]
2/1/1       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/2       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/3       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/4       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
```
+++
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK
```
Link_Status = active,
port_state = Forward,
LACP_Status = ready
```
+++
```
telnet@ICX6610-GDC# show trunk

Configured trunks:

Trunk ID: 257
Hw Trunk ID: 1
Ports_Configured: 5
Primary Port Monitored: Jointly

Ports   PortName Port_Status Monitor Rx_Mirr Tx_Mirr Monitor_Dir
2/1/1   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/2   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/3   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/4   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/5   FB-VDC*  disable     off     N/A     N/A     N/A

Operational trunks:

Trunk ID: 257
Hw Trunk ID: 1
Duplex: Full
Speed: 1G
Tag: Yes
Priority: level0
Active Ports: 4

Ports   Link_Status port_state LACP_Status
2/1/1   active      Forward    ready
2/1/2   active      Forward    ready
2/1/3   active      Forward    ready
2/1/4   active      Forward    ready
```
---

#### 5-1. インターフェースの有効化(LAGの設定ミスを考慮し、1ポートずつenableにする)

```
conf t
interface ethernet 2/1/5
 enable
```

+++

#### 5-2. インターフェースの期待値
enableにしたポートが以下の状態であればOK
```
Link = Up,
State = Forward,
Dupl = Full,
Speed = 1G,
Trunk = 257,
Tag = Yes,
MAC = 748e.f8e8.8a90
```
+++
```
telnet@ICX6610-GDC#show interfaces brief

Port    Link    State   Dupl Speed Trunk Tag Pvid Pri MAC            Name
2/1/1   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/2   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/3   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/4   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/5   Up      Forward Full 1G    257   Yes N/A  0   748e.f8e8.8a90  FB-VDC-B
2/1/6   Disable None    None None  None  No  1    0   748e.f8e8.8a95
2/1/7   Disable None    None None  None  No  1    0   748e.f8e8.8a96
2/1/8   Disable None    None None  None  No  1    0   748e.f8e8.8a97
2/1/9   Disable None    None None  None  No  1    0   748e.f8e8.8a98
2/1/10  Disable None    None None  None  No  1    0   748e.f8e8.8a99
2/1/11  Disable None    None None  None  No  1    0   748e.f8e8.8a9a
2/1/12  Disable None    None None  None  No  1    0   748e.f8e8.8a9b
2/1/13  Disable None    None None  None  No  1    0   748e.f8e8.8a9c
2/1/14  Disable None    None None  None  No  1    0   748e.f8e8.8a9d
2/1/15  Disable None    None None  None  No  1    0   748e.f8e8.8a9e
2/1/16  Disable None    None None  None  No  1    0   748e.f8e8.8a9f
2/1/17  Disable None    None None  None  No  1    0   748e.f8e8.8aa0
2/1/18  Disable None    None None  None  No  1    0   748e.f8e8.8aa1
2/1/19  Disable None    None None  None  No  1    0   748e.f8e8.8aa2
2/1/20  Disable None    None None  None  No  1    0   748e.f8e8.8aa3
2/1/21  Disable None    None None  None  No  1    0   748e.f8e8.8aa4
2/1/22  Disable None    None None  None  No  1    0   748e.f8e8.8aa5
2/1/23  Disable None    None None  None  No  1    0   748e.f8e8.8aa6
2/1/24  Disable None    None None  None  No  1    0   748e.f8e8.8aa7
2/2/1   Down    None    None None  None  No  N/A  0   748e.f8e8.8aa9
2/2/2   Down    None    None None  None  No  N/A  0   748e.f8e8.8aaa
2/2/6   Down    None    None None  None  No  N/A  0   748e.f8e8.8aab
2/2/7   Down    None    None None  None  No  N/A  0   748e.f8e8.8aac
2/3/1   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8aad  to-ICX66
2/3/2   Disable None    None None  None  No  1    0   748e.f8e8.8aae
2/3/3   Up      Forward Full 10G   None  No  184  0   748e.f8e8.8aaf  VDC-vMot
2/3/4   Up      Forward Full 10G   None  Yes N/A  0   748e.f8e8.8ab0  EX4200-G
2/3/5   Disable None    None None  None  No  1    0   748e.f8e8.8ab1
2/3/6   Disable None    None None  None  No  1    0   748e.f8e8.8ab2
2/3/7   Disable None    None None  None  No  1    0   748e.f8e8.8ab3
2/3/8   Disable None    None None  None  No  1    0   748e.f8e8.8ab4
mgmt1   Down    None    None None  None  No  None 0   748e.f8e8.8a90
```

+++
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK

```
[Sys P] = 1,
[Port P] = 1,
[Key] = 21100,
[Act] = Yes,
[Tio] = L,
[Agg] = Agg,
[Syn] = Syn,
[Col] = Col,
[Dis] = Dis,
[Def] = Def,
[Exp] = Exp,
[Ope] = Ope
```
+++
```
telnet@ICX6610-GDC#show link-aggregate
System ID: 748e.f8e8.8a90
Long  timeout: 90, default: 90
Short timeout: 3, default: 3
Port  [Sys P] [Port P] [  Key ] [Act][Tio][Agg][Syn][Col][Dis][Def][Exp][Ope]
2/1/1       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/2       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/3       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/4       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
2/1/5       1        1    21100   Yes   L   Agg  Syn  Col  Dis  No   No   Ope
```
+++
#### LAGのステータス確認
enableにしたポートの状態を確認し、以下の期待値であればOK
```
Link_Status = active,
port_state = Forward,
LACP_Status = ready
```
+++
```
telnet@ICX6610-GDC# show trunk

Configured trunks:

Trunk ID: 257
Hw Trunk ID: 1
Ports_Configured: 5
Primary Port Monitored: Jointly

Ports   PortName Port_Status Monitor Rx_Mirr Tx_Mirr Monitor_Dir
2/1/1   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/2   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/3   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/4   FB-VDC*  enable      off     N/A     N/A     N/A
2/1/5   FB-VDC*  enable      off     N/A     N/A     N/A

Operational trunks:

Trunk ID: 257
Hw Trunk ID: 1
Duplex: Full
Speed: 1G
Tag: Yes
Priority: level0
Active Ports: 5

Ports   Link_Status port_state LACP_Status
2/1/1   active      Forward    ready
2/1/2   active      Forward    ready
2/1/3   active      Forward    ready
2/1/4   active      Forward    ready
2/1/5   active      Forward    ready
```